**DeepLabCut:** a software package for animal pose estimation

![](deeplabcut_gif.gif)

DeepLabCut is a software package that helps quantify behavior in videos. It allows anyone the ability to track movement in videos. I picked this domain because I am interested in the applications of computer vision. It can be used to quantify videos which can help solve many problems.. DeepLabCut is very computational heavy because it trains deep neural networks on videos. I ran the DeepLabCut testscript on a CPU and a nvidia GPU. Because the testscript didn't take that much computation the CPU and GPU time were similar. 

**DeepLabCut Steps:**
-	Create a project, extract frames + label data in frames (training data)
-	Select + Train deep neural network
-	Evaluate network performance. 
-	Analyze video
-	Results 

**Conda installation instructions:**

wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh

//download conda installer

Chmod u+x Anaconda3-2020.11-Linux-x86_64.sh

//change permissions

./Anaconda3-2020.11-Linux-x86_64.sh

//Run installer

**Methodology**
DeepLabCut provides virtual conda-environment for the CPU and GPU. The main difference is that in the GPU environment, tensorflow-gpu==1.15.5 is loaded instead of tensorflow==1.15.5. You must create this virtual environment and then activate it before runnning any DeepLabCut code. To run the example testscript file, you must be in the examples folder. 

** CPU DeepLabCut instructions:**

git clone https://github.com/AlexEMG/DeepLabCut.git

//download deeplabcut repo on a folder

conda env create -f DLC-CPU-LITE.yaml

//create virtual environment

conda activate DLC-CPU-LITE

//activate virtual environment

conda install -c conda-forge ffmpeg


cd DeepLabCut/examples


python testscript.py

** GPU DeepLabCut instructions:**

git clone https://github.com/AlexEMG/DeepLabCut.git

//download deeplabcut repo on a folder

conda env create -f DLC-GPU-LITE.yaml

//create virtual environment

conda activate DLC-GPU-LITE

//activate virtual environment

conda install -c conda-forge ffmpeg


cd DeepLabCut/examples


python testscript.py

Timing Results:

These tests were run on the dev-intel14-k20 node.

(Average of 10 trials)

CPU:
real    2m10.627s
user    6m15.292s
sys     1m32.097s

Gpu:
real    2m08.178s
user    5m47.014s
sys     1m12.367


Here is an image of the GPU runnning, I used the command watch -n 1 nvidia-smi to display it.
<img src="gpu.png"> 

Overall, I learned a lot about how to install software on the HPCC. I learned about conda virtual environments and how they can be stored in yaml files, how to set my conda path properly, how to debug errors about incompatiable packages, and DeepLabCut. Some next steps would include reaching out to researchers who need data on videos and teaching them how to use the software. Another step would be to train the DeepLabCut model on a very large data set and comparing the GPU and CPU runtime. I was disssapointed that the testscript file that was provided wasn't large enough to show a difference in speed. 

**Error warning:**

TypeError: expected str, bytes or os.PathLike object, not NoneType

If you get that error, it is likely because you ran the testscript.py file again. Remove the DeepLabCut repository and download again. 

Sometimes the submission script doesn't work, in that case, delete the DeepLabCut repository and manually run the commands, it should work properly then.

**Sources:**

https://github.com/DeepLabCut/DeepLabCut/blob/master/conda-environments/README.md
