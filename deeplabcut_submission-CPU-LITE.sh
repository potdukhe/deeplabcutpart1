#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --mem=4gb
#SBATCH -c 2
#SBATCH -N 2
git clone https://github.com/AlexEMG/DeepLabCut.git
#download deeplabcut repo on a folder

conda activate DLC-CPU-LITE
#activate virtual environment
conda install -c conda-forge ffmpeg
pip install opencv-python-headless==3.4.8.29
pip install tensorpack==0.9.8
#install various packages

cd DeepLabCut/examples #go into the example folder
python testscript.py #run the test script
